#### Keras Implementation of the paper "CariGANs: Unpaired Photo-to-Caricature Translation"
• Authors: by KAIDI CAO; Tsinghua University, JING LIAO; City University of Hong Kong, Microsoft Research and LU YUAN, Microsoft AI Perception and Mixed Reality

• Arxiv: <https://arxiv.org/pdf/1811.00222.pdf>

#### Acknowledgements:
• Face warping from landmarks is from here: `https://github.com/marsbroshok/face-replace/blob/master/faceWarp.py`

• Implementation of GAN takes a lot of inspirations from many previous implementation I found on Github:


#### Data
Opensourced CelebA dataset is used for training.
<https://drive.google.com/drive/folders/0B7EVK8r0v71pWEZsZE9oNnFzTm8>
